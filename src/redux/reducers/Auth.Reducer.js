
const initState = {
  user: {},
};

export const AuthReducer = (state = initState, action) => {
  switch (action.type) {
    case "LOGIN":
      return {
        user: action.payload,
      };
    default:
      return state;
  }
};
