import logo from './logo.svg';
import './App.css';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import LoginPage from './pages/LoginPage';
import { Route, Switch } from 'react-router-dom';
import LandingPage from './pages/LandingPage';

function App() {
  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#ba4b4b",
        contrastText: "#ffffff",
      },
    },
  });
  return (
    <div className="App">
     <MuiThemeProvider theme={theme}>
<Switch>
  <Route exact path="/" component={LoginPage} />
  <Route path="/home" component={LandingPage} />
</Switch>
     </MuiThemeProvider>
    </div>
  );
}

export default App;
