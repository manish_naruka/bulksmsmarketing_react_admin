import React, { Component } from "react";
import { Button, TextField } from "@material-ui/core";

export class LoginPage extends Component {
  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          alignItems: "center",
          height: "30%",
          alignSelf: "center",

          marginTop: "10%",
        }}
      >
        <TextField
          id="outlined-basic"
          style={{ paddingTop: "50px" }}
          label="Email"
          variant="outlined"
        />
        <TextField
          id="outlined-basic"
          style={{ paddingTop: "50px", height: "30px" }}
          label="Password"
          variant="outlined"
        />

        <Button
          onClick={() => {
            this.props.history.push("/home");
          }}
          style={{ marginTop: "50px" }}
          variant="contained"
          color="primary"
        >
          Login
        </Button>
      </div>
    );
  }
}

export default LoginPage;
